extends Control

var timeout = 2.0

func _process(delta):
	timeout -= delta
	if timeout < 0:
		get_tree().change_scene("res://maps/map_01.tscn")
