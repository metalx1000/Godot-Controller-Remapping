extends Sprite

var speed = 300
var shoot_timeout_start = .5
var shoot_timeout = 0

export var player_id = 0
var lasers = preload("res://objects/laser/laser.tscn")

func _process(delta):
	if Input.is_joy_button_pressed(player_id,Controls.buttons.LEFT):
		position.x -= speed * delta
	elif Input.is_joy_button_pressed(player_id,Controls.buttons.RIGHT):
		position.x += speed * delta

	if Input.is_joy_button_pressed(player_id,Controls.buttons.UP):
		position.y -= speed * delta
	elif Input.is_joy_button_pressed(player_id,Controls.buttons.DOWN):
		position.y += speed * delta
		
	shoot_timeout -= delta
	if Input.is_joy_button_pressed(player_id,Controls.buttons.SHOOT):
		if shoot_timeout <= 0:
			shoot_timeout = shoot_timeout_start
			var laser = lasers.instance()
			laser.position = position
			get_tree().get_current_scene().add_child(laser)
	
