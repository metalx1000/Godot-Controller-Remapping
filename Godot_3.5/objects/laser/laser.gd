extends Sprite

var speed = 300

func _process(delta):
	position.y -= speed * delta
	
	if position.y < -100:
		queue_free()
